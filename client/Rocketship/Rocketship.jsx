//window.scrollTo(0,document.body.scrollHeight);
import FullPage from "Components/LandingPage/FullPage.jsx"

Rocketship = React.createClass({
  render() {
    return (
        <div>
      <FullPage color="red" />
      {this.renderEnvi()}
        </div>
    )
  },

  renderEnvi() {
      return this.getColors().map((color) =>{
        return <Fullpage sid={color.sid} color={color} />;
      });
  },

  getColors() {
    return [
      {sid: "zero", color: "280F36"},
      {sid: "one", color: "632B6C"},
      {sid: "two", color: "C86B98"},
      {sid: "three", color: "F09F9C"},
      {sid: "four", color: "FFC1A0"},
      {sid: "five", color: "F8EDCD"},
      {sid: "six", color: "E4E2C9"},
      {sid: "seven", color: "BAC6B0"},
      {sid: "eight", color: "5B3930"},
      {sid: "nine", color: "7D6A66"},

    ];
  },


})
