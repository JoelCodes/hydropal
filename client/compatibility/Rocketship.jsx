//window.scrollTo(0,document.body.scrollHeight);

Rocketship = React.createClass({
  render() {
    return (
        <div>
        {this.renderEnvi()}
        <img onClick={this.fly} style={this.rocketCss()} id="rocket" src="http://vignette2.wikia.nocookie.net/scribblenauts/images/7/7d/Starship.png/revision/latest?cb=20130119142644" />
        </div>
    )
  },

  renderEnvi() {
      return this.getColors().map((color) =>{
        return <FullPage key={color.sid} sid={color.sid} color={color.color} />;
      });
  },


  fly() {
    var $slide = $("#nThree");
    var $root = $("html body");
    var $r = $("#rocket");
    var $fire = $('#fire');
    $root.animate({
      scrollTop: $slide.offset().top
    },6000), function() {  console.log("Scrolled to space");
    };

    var fly = new TimelineLite({paused:false});

    fly.to($r,9,
      {
        bottom: "400px",
        ease: Power1.easeOut
      }).to($r, 20,
          {
            rotation: "360",
            repeat: 0,
            ease: Power0.easeNone,
            scale : 0
          })




  },


  getColors() {
    return [
      {sid: "nThree", color: "black"},
      {sid: "nTwo", color:"#1c0021"},
      {sid: "nOne", color:"#240047"},
      {sid: "zero", color: "#280F36"},
      {sid: "one", color: "#632B6C"},
      {sid: "two", color: "#C86B98"},
      {sid: "three", color: "#F09F9C"},
      {sid: "four", color: "#FFC1A0"},
      {sid: "five", color: "#F8EDCD"},
      {sid: "six", color: "#E4E2C9"},
      {sid: "seven", color: "#BAC6B0"},
      {sid: "eight", color: "#5B3930"},
      {sid: "nine", color: "#7D6A66"},

    ];
  },

  componentDidMount() {
    var $r = $("#rocket")
    var $slide = $("#nine");
  var $root = $("html body");
    $root.animate({
      scrollTop: $slide.offset().top
    },3000), function() {  console.log("Scrolled to bottom");
  };

  TweenLite.from($r, 4, {opacity:0, bottom:"300px",onComplete: function() {
    TweenMax.to($r, 1, {bottom:"20px",
                       repeat:-1, yoyo:true});
  }});
},

  rocketCss() {
    return {
      position: "fixed",
      zIndex: 10,
      bottom: 0,
      left: "50%",
      transform: "translate(-50%,-50%)",
      opacity: 1

    }
  },

  fireCss() {
    return {
      position: "fixed",
      zIndex: 9,
      bottom: -78,
      left: "40%",
      transform: "translate(-50%,-50%)",
      height: "300px",
      width: "100px",
      transform: "rotatex(180deg)"
    }
  }


})
