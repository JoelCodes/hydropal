DRWI = React.createClass({



  getInitialState() {
    return {
        bodyWeight: 160,
        waterDrank: 0,
        totalWater: 0
    }
  },

  render() {
    return(
      <div className="RDWI">

        <div className="row">
          <div className="col-sm-12 text-center">
            <h1 id="logo" style={this.getHeadingCss()}>Hello, {this.data.name}</h1>
          </div>
        </div>
      <div className="row text-center">
        <div style={this.getDRWICss()} className="holder text-center">
          <h1 style={this.getPercentCss()} >{this.calcDRWI()}</h1>
        </div>
      </div>
        <div className="row text-center">
          <div className="col-sm-4 col-sm-offset-4 text-center">
            <AddWaterBtn clickHandler={this.addWater8} amount="8" />
            <AddWaterBtn clickHandler={this.addWater16} amount="16" />
          </div>
        </div>



      </div>
    );
  },


  calcDRWI() {
    var oz =  (this.state.bodyWeight / 2);
    var remaining = this.state.waterDrank / oz * 100;
    return remaining + "%"
  },

  addWater8() {
    this.setState({waterDrank: this.state.waterDrank + 8 });
  },

  addWater16() {
    this.setState({waterDrank: this.state.waterDrank + 16 });
  },


  componentDidMount() {

    $("*").hide().fadeIn();


  },

  getPercentCss() {
    return {

    }
  },

  getDRWICss() {
      return {
        borderRadius: "50%",
        border: "1px solid black",
        width: "100px",
        height: "100px"
      }
  },

  getHeadingCss() {
    return {
      paddingTop: "3em"

    }
  }
})
