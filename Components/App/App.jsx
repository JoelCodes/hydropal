App = React.createClass({

  mixins: [ReactMeteorData],

  getMeteorData() {
    return {
      name: Meteor.user().username
    }
  },


  render() {
    return (
        <div style={this.getAppCss()} className="app">
        <ButtonWrapper />
        </div>
  );
},


getAppCss() {
  return {
    position: 'relative',
    height: '100vh',
    width: "100vw",
    background: "#FEF9F0"
  }
},

componentDidMount() {
  $("*").fadeIn();
}

})
