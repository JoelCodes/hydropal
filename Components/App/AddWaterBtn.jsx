AddWaterBtn = React.createClass({
  render() {
  return (
    <button style={this.getBtnCss()} onClick={this.props.clickHandler}
     type="button" className=" btn-primary-outline">{this.props.amount} Oz</button>
  )
},

getBtnCss() {
  return {
    background: 'transparent'
  }
}

})
