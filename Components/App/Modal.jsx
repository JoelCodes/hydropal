Modal = React.createClass({
  render() {
    return(
      <section>
      <div className="col-12 center-text">
        <h1> Welcome {this.props.name} </h1>
        <img src="images/cloud.png" />


<div id="modal1" className="modal">
  <div className="modal-content text-center">
    <h4>Lets get you started</h4>
    <p>Enter Body Weight below so we may calculate your daily water recommendation</p>
    <div className="row">
         <div className="input-field col s12">
           <input id="BodyWeight" type="text" className="validate"> </input>
           <label for="BodyWeight">Body Weight</label>
         </div>
       </div>
  </div>

    <a onClick={this.onSubmit()} href="#modal1" className=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
</div>

      </div>
      </section>
    )
  },

  onSubmit() {

    var $input = ("#BodyWeight").val;

    Meteor.users.update(Meteor.userId(), {$set: {"profile.body": $input}});

  }
})
