ButtonWrapper = React.createClass({
  render() {
    return (
      <div style={this.getSubCss()} >
        <AccountsUIWrapper />
        <span id="logo" style={this.getSpanCss()}>Hydro<small style={this.getSmallCss()}>Pal</small></span>
      </div>
  );
},
getSubCss() {
  return {
    width: "100%",
    background: "rgba(0, 0, 0,0.2)",
    float: "left",
    position: "fixed",
    top: "0",
    padding: '10px',
    zIndex: 100
  }
},
getSpanCss() {
  return {
    fontSize: "26px",
    color: "white",
    fontWeight: "600",
    float: "right"
  }
},

getSmallCss() {
  return {
    fontWeight: "200",
    color: "white"
  }
}

})
