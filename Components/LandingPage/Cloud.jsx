Cloud = React.createClass({
  render() {
    return (
      <img style={this.getCss()} id="cloud" src="http://images.clipartpanda.com/cloudy-clipart-black-and-white-white-cloud-md.png" />
    )
  },

  componentDidMount() {
    $(function() {
      var totalElements = 0;
      var elementAnimationDelay = 0.4;
      var elementCountUp = 0;

      var $cloud = $("#cloud");

      function elementFloat(elementItem) {
        TweenMax.to(elementItem, 2, {
          delay: elementAnimationDelay,
          y: "+=20px",
          yoyo: true,
          repeat: -1,
          ease: Power2.easeInOut
    });
  }
  elementFloat($cloud);

})

  },

  getCss() {
    return {
      position: 'fixed',
      top: this.props.top

    }
  }


})
