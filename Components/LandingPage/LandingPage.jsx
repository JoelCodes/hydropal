LandingPage = React.createClass({

  getInitialState() {
    return {
      slide: 1
    }
  },



  slideTo2() {
      var $slide = $("#slide2");

    console.log("hello slide2");
    var $root = $("html body");
      $root.animate({
        scrollTop: $slide.offset().top
      },1000);

      $c = ("#cloud");
      TweenLite.to($c, 3, {left:"80%", ease:Bounce.easeOut});

  },

  slideTo3() {
      var $slide = $("#slide3");

    var $root = $("html body");
      $root.animate({
        scrollTop: $slide.offset().top
      },1000);

      $c = ("#cloud");
      TweenLite.to($c, 3, {left:"0", ease:Bounce.easeOut});
      console.log("hello slide3");


  },

  slideTo1() {
      var $slide = $("#slide1");

    console.log("hello slide 1");
    var $root = $("html body");
      $root.animate({
        scrollTop: $slide.offset().top
      },1000);

      $c = ("#cloud");
      TweenLite.to($c, 3, {left:"80%", ease:Bounce.easeOut});

  },

  render() {
    return(
      <div>
        <ButtonWrapper ref="header" />
        <Cloud top="70%" />
        <FullPage onClick={this.slideTo2} ref="slide1" sid="slide1" text="Hi, I'll be your HydroPal. We'll be going on this water filled adventure
        together!" color="#206BA4" />
        <FullPage onClick={this.slideTo3} ref="slide2" sid="slide2" text="Are These settings
        okay?" color="orange" />
        <FullPage onClick={this.slideTo1} ref="slide3" sid="slide3" text="Lets calculate your
        daily recommended water intake" color="#00cdcd" />
      </div>
    )
  },

  componentDidMount() {
    $("*").hide().fadeIn()

  }




})
