Heading = React.createClass({

  render() {
    return(
  <div className="row">
    <div className="col-sm-8 col-sm-offset-2">
      <h1 style={this.geth1Css()}>{this.props.text}</h1>
    </div>
  </div>  
    )
  },

  geth1Css() {
    return {
      color: "white",
      fontWeight: "400",
      lineHeight: '1.5',
      fontSize: "65px",
      paddingTop: "50px"

    }
  }


})
