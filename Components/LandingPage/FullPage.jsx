

FullPage = React.createClass({
  render() {
    return (
      <section onClick={this.props.onClick}  style={this.getSectionCss()}>
          <div id={this.props.sid} className="slide text-center">

              <Heading text={this.props.text} />
          </div>


      </section>
    )
  },

  getSectionCss() {
    var color = this.props.color
    return {
      backgroundColor: color,
      height: "105vh",
      width: "100vw",
      paddingTop: "20px",

    }
  }

});
