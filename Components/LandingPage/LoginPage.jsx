// App component - represents the whole app

LoginPage = React.createClass({

  mixins: [ReactMeteorData],

  getMeteorData() {
    return {
      currentUser: Meteor.user()
    }
  },

  render() {

    return (
        <div  style={this.getHeaderCss()}>

      <span style={this.getSpanCss()}>Hydro<small style={this.getSmallCss()}>Pal</small><ButtonWrapper /></span>

        </div>

    );
  },

  getHeaderCss() {
    return {
      background: "#1693A5",
      position: "relative",
      width: "100%",
      height: "auto"


    }
  },

  getSpanCss() {
    return {
      fontSize: "50px",
      color: "#C7EDE8",
      fontWeight: "600"
    }
  },

  getSmallCss() {
    return {
      fontWeight: "200",
      color: "white",
      fontSize: "40px"
    }
  }

});
