NextBtn = React.createClass({
  render() {
    return (
  <div className="row  text-center">
      <p style={this.getIconCss()} className="glyphicon glyphicon-arrow-down"></p>
  </div>
  )
  },

  getIconCss() {
      return {
        fontSize: '40px',
        borderRadius: "100%",
        color:  "white"
      }
  }
});
